

describe("Currency Converter", function() {

    it("converts 100 USD to EUR", function() {
        expect(convert("USD", "EUR", 100)).toBeCloseTo(92);
    });

    it("converts 50 EUR to USD", function() {
        expect(convert("EUR", "USD", 50)).toBeCloseTo(54);
    });

    it("converts 20 USD to TRY", function() {
        expect(convert("USD", "TRY", 20)).toBeCloseTo(529);
    });

    it("converts 100 TRY to USD", function() {
        expect(convert("TRY", "USD", 100)).toBeCloseTo(3.7);
    });

    it("converts 100 EUR to RUB", function() {
        expect(convert("EUR", "RUB", 100)).toBeCloseTo(10214);
    });

    it("converts 1000 RUB to EUR", function() {
        expect(convert("RUB", "EUR", 1000)).toBeCloseTo(9);
    });

    it("throws an error for unsupported currencies", function() {
        expect(function() { 
            convert("USD", "GBP", 100); 
        }).toThrowError("Unsupported currency pair");
    });

});


describe("HTML layout tests", function() {
    beforeEach(function() {
        // Load the fixture at the start of each test
        loadFixtures('../../../index.html');
    });

    it("should contain two combo boxes", function() {
        let selectElements = $('select');
        expect(selectElements.length).toBe(2);
    });

    it("checks if each select contains at least 4 elements", function() {
        $('select').each(function() {
            let numberOfOptions = $(this).find('option').length;
            expect(numberOfOptions).toBeGreaterThanOrEqual(4);
        });
    });

    it("checks if the title is correct", function() {
        let fixtureContent = $('#jasmine-fixtures').html(); 
        let titleMatch = fixtureContent.match(/<title>(.*?)<\/title>/);
        let pageTitle = titleMatch ? titleMatch[1] : "";
        expect(pageTitle).toBe("Currency converter");
    });

    it("checks if the heading exists", function() {
        expect($("h1")).toExist();
        expect($("h1").text()).toBe("Hello, World!");
    });

    it("checks if options are the same in both combo boxes", function() {
        let selects = $('select');
        
        // Ensure there are exactly two combo boxes
        expect(selects.length).toBe(2);
    
        let firstSelectOptions = selects.eq(0).find('option').map(function() {
            return $(this).val();
        }).get();
    
        let secondSelectOptions = selects.eq(1).find('option').map(function() {
            return $(this).val();
        }).get();
    
        expect(firstSelectOptions).toEqual(secondSelectOptions);
    });
    
    it("checks if the select name is 'from_currency'", function() {
        let fromCurrencySelect = $('select#from_currency');
        expect(fromCurrencySelect.length).toBe(1);
    });
    
    it("checks if the select name is 'to_currency'", function() {
        let toCurrencySelect = $('select#to_currency');
        expect(toCurrencySelect.length).toBe(1);
    });

    it("checks if an input with the name 'amount' exists", function() {
        let amountInput = $('input#amount');
        expect(amountInput.length).toBe(1);
    });

    it("validates the input pattern against some sample values", function() {
        let amountInput = $('input#amount');
    
        amountInput.val("10.12");
        expect(amountInput[0].checkValidity()).toBeTruthy();
    
        amountInput.val("-10.12");
        expect(amountInput[0].checkValidity()).toBeFalsy();
    
        amountInput.val("10.123");
        expect(amountInput[0].checkValidity()).toBeFalsy();
    
        amountInput.val("abcd");
        expect(amountInput[0].checkValidity()).toBeFalsy();
    });

    it("checks if function is called on input and action", function() {
        spyOn(window, 'convertCurrency');
        $('input#amount').val('100');
        $('button#convertButton').click();
    
        expect(window.convertCurrency).toHaveBeenCalled();
    });
    
    it("checks if the result div is updated correctly", async function() {
        // spyOn(window, 'convertCurrency');
        $('input#amount').val('200');
        $('button#convertButton').click();
        expect($('div#result').text()).toBe('200');
        
    });
});
